# HereKitty

HereKitty is a C# library for interacting with The Cat API. It allows users to retrieve cat images, breed information, and more through a simple and intuitive interface. Developed by Diego Crespo, this library is distributed under the Apache 2.0 License.

## Features

- Fetch a list of cat images with various filtering options
- Retrieve a random cat image
- Get detailed information on cat breeds
- Download cat images directly to your local system

## Installation
It will be on NuGet eventually

## Usage

Below are some examples of how to use the `HereKitty` library:

### Initializing the API

```csharp
var apiKey = "YOUR_API_KEY";
var hereKitty = new HereKitty.HereKitty(apiKey);

//Getting a Random Cat Image

var randomCat = await hereKitty.GetRandomCatAsync();

//Downloading an Image

var homePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
var downloadsPath = Path.Combine(homePath, "Downloads", "CatPic.jpg");
await kitty.DownloadImageAsync(randomCat.Url, downloadsPath);

//Fetching Cat multiple Cat Images as a list

var cats = await hereKitty.GetCatsAsync(limit: 5, order: "ASC");

//Retrieving Cat Breeds

var breeds = await hereKitty.GetBreedAsync();

Contributing

Contributions are welcome. Please open an issue or submit a pull request.
License

This project is licensed under the Apache License 2.0 - see the LICENSE file for details.
Acknowledgments

    The Cat API for providing the data.
    Diego Crespo for developing this library.