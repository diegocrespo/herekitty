namespace HereKitty;

/// <summary>
///     The main format returned by API
/// </summary>
public class Cat
{
    /// <summary>
    ///     ids of all the cats. I.E beng, asho
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    ///     url to the jpg of the cat
    /// </summary>
    public string Url { get; set; }

    /// <summary>
    ///     Width in Pixels
    /// </summary>
    public int Width { get; set; }

    /// <summary>
    ///     Height in Pixels
    /// </summary>
    public int Height { get; set; }
}

/// <summary>
///     For the v1 breeds endpoint
/// </summary>
public class Breed
{
    public Weight Weight { get; set; }
    public string Id { get; set; }
    public string Name { get; set; }
    public string CfaUrl { get; set; }
    public string VetstreetUrl { get; set; }
    public string VcahospitalsUrl { get; set; }
    public string Temperament { get; set; }
    public string Origin { get; set; }
    public string CountryCodes { get; set; }
    public string CountryCode { get; set; }
    public string Description { get; set; }
    public string LifeSpan { get; set; }
    public int Indoor { get; set; }
    public int Lap { get; set; }
    public string AltNames { get; set; }
    public int Adaptability { get; set; }
    public int AffectionLevel { get; set; }
    public int ChildFriendly { get; set; }
    public int DogFriendly { get; set; }
    public int EnergyLevel { get; set; }
    public int Grooming { get; set; }
    public int HealthIssues { get; set; }
    public int Intelligence { get; set; }
    public int SheddingLevel { get; set; }
    public int SocialNeeds { get; set; }
    public int StrangerFriendly { get; set; }
    public int Vocalisation { get; set; }
    public int Experimental { get; set; }
    public int Hairless { get; set; }
    public int Natural { get; set; }
    public int Rare { get; set; }
    public int Rex { get; set; }
    public int SuppressedTail { get; set; }
    public int ShortLegs { get; set; }
    public string WikipediaUrl { get; set; }
    public int Hypoallergenic { get; set; }
    public string ReferenceImageId { get; set; }

    public override string ToString()
    {
        return $"ID: {Id}, Name: {Name}";
    }
}

public class Weight
{
    public string Imperial { get; set; }
    public string Metric { get; set; }
}