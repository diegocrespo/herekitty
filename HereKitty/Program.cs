﻿using System.Text.Json;

namespace HereKitty;

internal class Program
{
    private static readonly HttpClient client = new();

    private static async Task Main(string[] args)
    {
        // Read API key from JSON file
        var filePath = "/home/deca/auth/cat_api.json";
        using var doc = JsonDocument.Parse(File.ReadAllText(filePath));
        var apiKey = doc.RootElement.GetProperty("api_key").GetString();
        var kitty = new HereKitty(apiKey);
        //var response = await kitty.GetCatsAsync(limit: 5, breedIds: "beng");
        var response = await kitty.GetBreedAsync();

        //var response = await kitty.GetRandomCatAsync();
        Console.WriteLine($"value={response}");
        foreach (var breed in response) Console.WriteLine(breed);
    }
}