using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;

namespace HereKitty;

/// <summary>
///     Class that contains functions for the various endpoints
/// </summary>
public class HereKitty
{
    private readonly string _baseUrl = "https://api.thecatapi.com/v1/";
    private readonly HttpClient _httpClient;
    private readonly string apiKey;

    /// <summary>
    ///     Initializes a new instance of the <see cref="HereKitty" /> class with a specified API key.
    /// </summary>
    /// <param name="apiKey">The API key for The Cat API.</param>
    public HereKitty(string apiKey)
    {
        this.apiKey = apiKey;
        _httpClient = new HttpClient();

        _httpClient.BaseAddress = new Uri(_baseUrl);
        _httpClient.DefaultRequestHeaders.Accept.Clear();
        _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("X-API-Key", this.apiKey);
    }

    /// <summary>
    ///     Retrieves a list of cats with optional query parameters.
    /// </summary>
    /// <param name="limit">Number of cat images to return. Values are 1-100. Optional.</param>
    /// <param name="page">Page number for pagination. Values are 0-n. Optional.</param>
    /// <param name="order">Order to return the images in (ASC/DESC/RAND). Optional.</param>
    /// <param name="hasBreeds">Filter to only return images with breed information. Values are 0 or 1 Optional.</param>
    /// <param name="breedIds">Comma-separated list of breed IDs to filter images. I.E beng, asho. Optional.</param>
    /// <param name="categoryIds">Comma-separated list of category IDs to filter images. I.E 1,10,11. Optional.</param>
    /// <param name="subId">Filter images by the sub_id value used when uploading. Optional.</param>
    /// <returns> Asynchronously returning a list of <see cref="Cat" /> objects.</returns>
    public async Task<List<Cat>> GetCatsAsync(int? limit = null, int? page = null, string order = null,
        bool? hasBreeds = null, string breedIds = null, string categoryIds = null, string subId = null)
    {
        var queryParams = new StringBuilder("images/search?");
        // Set up the additional query Params
        if (limit.HasValue) queryParams.Append($"limit={limit.Value}&");
        if (page.HasValue) queryParams.Append($"page={page.Value}&");
        if (!string.IsNullOrWhiteSpace(order)) queryParams.Append($"order={WebUtility.UrlEncode(order)}&");
        if (hasBreeds.HasValue) queryParams.Append($"has_breeds={(hasBreeds.Value ? 1 : 0)}&");
        if (!string.IsNullOrWhiteSpace(breedIds)) queryParams.Append($"breed_ids={WebUtility.UrlEncode(breedIds)}&");
        if (!string.IsNullOrWhiteSpace(categoryIds))
            queryParams.Append($"category_ids={WebUtility.UrlEncode(categoryIds)}&");
        if (!string.IsNullOrWhiteSpace(subId)) queryParams.Append($"sub_id={WebUtility.UrlEncode(subId)}&");

        // Trim the last '&' or '?' if it's the last character
        if (queryParams[^1] == '&' || queryParams[^1] == '?') queryParams.Length--;

        var response = await _httpClient.GetFromJsonAsync<List<Cat>>(queryParams.ToString());
        return response;
    }

    /// <summary>
    ///     Retrieves a random cat image.
    /// </summary>
    /// <returns>Asynchronously returning a random <see cref="Cat" /> object.</returns>
    public async Task<Cat> GetRandomCatAsync()
    {
        var cats = await _httpClient.GetFromJsonAsync<List<Cat>>("images/search");
        return cats[0];
    }

    /// <summary>
    ///     Retrieves a list of cat breeds.
    /// </summary>
    /// <returns>Asynchronously returning a list of <see cref="Breed" /> objects.</returns>
    public async Task<List<Breed>> GetBreedAsync()
    {
        var breeds = await _httpClient.GetFromJsonAsync<List<Breed>>("breeds");
        return breeds;
    }

    /// <summary>
    ///     Downloads a cat image from a specified URL to a specified file path.
    /// </summary>
    /// <param name="imageUrl">The URL of the image to download.</param>
    /// <param name="filePath">The local file path to save the image to.</param>
    /// <returns>Asynchronous write to the specified file.</returns>
    public async Task DownloadImageAsync(string imageUrl, string filePath)
    {
        using (var response = await _httpClient.GetAsync(imageUrl, HttpCompletionOption.ResponseHeadersRead))
        {
            response.EnsureSuccessStatusCode();
            using (var stream = await response.Content.ReadAsStreamAsync())
            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                await stream.CopyToAsync(fileStream);
            }
        }
    }
}